<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m210324_184025_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(11),
            'agent_id' => $this->integer(11),
            'request_id' => $this->integer(11),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->null(),
            'deadline' => $this->date()->notNull(),
            'agent_cash' => $this->string(3)->notNull(),
            'status' => $this->string(50)->defaultValue('В поиске исполнителя'),
        ]);

        $this->createTable('{{%requests}}', [
            'id' => $this->primaryKey(11),
            'order_id' => $this->integer(11)->unique(),
            'user_id' => $this->integer(11),
            'status' => $this->string(50)->null(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%orders}}');
        $this->dropTable('{{%requests}}');

    }
}
