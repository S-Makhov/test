<?php

use yii\db\Migration;

/**
 * Class m210324_054814_update_user_table
 */
class m210324_054814_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'name', $this->string(30));
        $this->addColumn('{{%user}}', 'phone', $this->string(15));
        $this->addColumn('{{%user}}', 'type', $this->string(30));
        $this->dropColumn('{{%user}}', 'email');


        $model = new \common\models\User();
        $model->generateAuthKey();
        $model->setPassword('adminadmin');
        $model->generatePasswordResetToken();

        $this->insert('{{%user}}', [
            'id'                   => 1,
            'username'             => 'admin',
            'auth_key'             => $model->auth_key,
            'password_hash'        => $model->password_hash,
            'password_reset_token' => $model->password_reset_token,
            'status'               => 10,
            'created_at'           => time(),
            'updated_at'           => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'name');
        $this->dropColumn('{{%user}}', 'phone');
        $this->dropColumn('{{%user}}', 'type');
        $this->addColumn('{{%user}}', 'email', $this->string(30));

        return false;
    }

}
