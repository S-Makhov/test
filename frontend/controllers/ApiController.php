<?php

namespace frontend\controllers;

use common\models\Api;
use common\models\Orders;
use common\models\Requests;
use common\models\User;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use Yii;


class ApiController extends Controller
{

    public function init()
    {
        parent::init();
        Yii::$app->user->logout();
        Yii::$app->session->destroy();
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'auth' => function ($username, $password) {
                if (($user = User::find()->where(['username' => $username])->one()) && ($user->validatePassword($password))) {
                    return $user;
                } else {
                    return NULL;
                }
            },
        ];

        return $behaviors;
    }



    /**************/
    /**   USER   **/
    /**************/
    public function actionUser($action){
        $request= \Yii::$app->getRequest()->bodyParams;

        if ($action == 'create') {
            $user = new User();

            $user->username = $request['username'];
            $user->password_hash = Yii::$app->security->generatePasswordHash($request['pass']);
            $user->auth_key = Yii::$app->security->generateRandomString();
            $user->name = $request['name'];
            $user->phone = str_replace(array('+', ' ', '(' , ')', '-'), '', $request['phone']);
            $user->type = $request['type'];
            $user->created_at = time();

            return $user->save()? ['success' => 1] : ['success' => 0, 'error' => $user->errors];
        }

        return false;
    }


    /**************/
    /**  заказы  **/
    /**************/
    public function actionOrder($action){
        $request = \Yii::$app->getRequest()->bodyParams;

        // создать заказ
        if ($action == 'create') {
            $model = new Orders();

            $model->agent_id = Yii::$app->user->id;
            $model->title = $request['title'];
            $model->description = $request['description'];
            $model->deadline = !empty(trim($request['deadline']))? date('Y-m-d', strtotime($request['deadline'])) : null;
            $model->agent_cash = $request['agent_cash'];

            return $model->save()? ['success' => 1, 'order_id' => $model->id] : ['success' => 0, 'error' => $model->errors];
        }

        // Закрыть заказ
        if ($action == 'close') {
            if (!empty($request['order_id'])) {
                $order = Orders::find()->with('request')->where(['id' => $request['order_id']])->one();
                if (!is_null($order)) {
                    if (Yii::$app->user->id == $order->agent_id) {
                        if ($order->status == 'В работе') {

                            $order->status = 'Выполнен';
                            $order->request->status = 'Выполнен';

                            if ($order->save() && $order->request->save()){
                                return ['success' => 1];
                            } else {
                                $err['order'] = $order->errors;
                                $err['request'] = $request->errors;

                                return ['success' => 0, 'error' => $err];
                            }
                        } else {
                            return ['success' => 0, 'error' => 'Завершить заказ можно только для статуса - В работе', 'status' => $order->status];
                        }
                    } else {
                        return ['success' => 0, 'error' => 'Текущий пользователь не является создателем заказа'];
                    }
                } else {
                    return ['success' => 0, 'error' => 'Данного заказа не существует'];
                }
            } else {
                return ['success' => 0, 'error' => 'order_id is empty'];
            }
        }

        return false;
    }



    /**************/
    /**  заявки  **/
    /**************/
    public function actionRequest($action){
        $request = \Yii::$app->getRequest()->bodyParams;

        // Создать заявку
        if ($action == 'create') {
            $model = new Requests();

            $orders = Orders::find()->where(['id' => trim($request['order_id'])])->one();
            if (!is_null($orders)) {
                if ($orders->agent_id != Yii::$app->getUser()->id){
                    $model->order_id = $request['order_id'];
                    $model->user_id = Yii::$app->getUser()->id;

                    return $model->save()? ['success' => 1, 'request_id' => $model->id] : ['success' => 0, 'error' => $model->errors];
                } else {
                    return ['success' => 0, 'error' => 'Посредник не может создать заявку на свой заказ'];
                }
            } else {
                return ['success' => 0, 'error' => 'Заказа с данным номером не существует'];
            }
        }

        // Принять заявку
        if ($action == 'accept') { // только для заказов со статусом в поиске

            if (!empty($request['order_id'])) {
                $requests = Requests::find()->where(['order_id' => $request['order_id']])->one();
                if (!is_null($requests)) {
                    $order = Orders::find()->where(['id' => $request['order_id']])->one();

                    if (Yii::$app->user->id == $order->agent_id) {
                        $order->request_id = $requests->id;
                        $order->status = 'В работе';
                        $requests->status = 'В работе';
                        if ($order->save() && $requests->save()){
                            return ['success' => 1];
                        } else {
                            $err['order'] = $order->errors;
                            $err['request'] = $requests->errors;

                            return ['success' => 0, 'error' => $err];
                        }
                    } else {
                        return ['success' => 0, 'error' => 'Текущий пользователь не является создателем заказа'];
                    }
                } else {
                    return ['success' => 0, 'error' => 'Заявки к данному заказу не существует'];
                }
            } else {
                return ['success' => 0, 'error' => 'order_id is empty'];
            }
        }

        return false;
    }
}
