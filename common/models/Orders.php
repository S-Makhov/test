<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $agent_id
 * @property int|null $request_id
 * @property string $title
 * @property string|null $description
 * @property string $deadline
 * @property string $agent_cash
 * @property string|null $status
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agent_id', 'request_id'], 'integer'],
            [['title', 'deadline', 'agent_cash'], 'required'],
            [['description', ], 'string'],
            [['deadline'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['agent_cash'], 'string', 'max' => 3],
            [['status'], 'string', 'max' => 50],
            [['request_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_id' => 'Agent ID',
            'request_id' => 'Request ID',
            'title' => 'Title',
            'description' => 'Description',
            'deadline' => 'Deadline',
            'agent_cash' => 'Agent Cash',
            'status' => 'Status',
        ];
    }


    public function getRequest()
    {
        return $this->hasOne(Requests::className(), ['order_id' => 'id']);
    }
}
